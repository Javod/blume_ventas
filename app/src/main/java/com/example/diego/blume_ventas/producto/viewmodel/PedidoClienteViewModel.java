package com.example.diego.blume_ventas.producto.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.producto.repository.PedidoClienteRepository;

import java.util.List;

public class PedidoClienteViewModel extends AndroidViewModel {

    private PedidoClienteRepository pedidoClienteRepository;
    private LiveData<List<PedidoCliente>> mPedidosCliente;


    public PedidoClienteViewModel(@NonNull Application application) {
        super(application);

        pedidoClienteRepository = new PedidoClienteRepository(application);
        mPedidosCliente = pedidoClienteRepository.getPedidosCliente();
    }

    public LiveData<List<PedidoCliente>> getPedidosClientes()
    {
        return  mPedidosCliente;
    }

    public void insertPedido(PedidoCliente pedidoCliente)
    {
        pedidoClienteRepository.insertPedidoCliente(pedidoCliente);
    }

    public  void actualizarPedido(PedidoCliente pedidoCliente)
    {
        pedidoClienteRepository.ActualizarPedido(pedidoCliente);
    }
}
