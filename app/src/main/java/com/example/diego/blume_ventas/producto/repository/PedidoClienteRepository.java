package com.example.diego.blume_ventas.producto.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.daos.PedidoClienteDAO;
import com.example.diego.blume_ventas.producto.database.ProductoDatabase;

import java.util.List;

public class PedidoClienteRepository {

    private PedidoClienteDAO pedidoClienteDAO;
    private LiveData<List<PedidoCliente>> mPedidoCliente;

    public PedidoClienteRepository(Application application)
    {
        ProductoDatabase db = ProductoDatabase.getDatabase(application);
        pedidoClienteDAO = db.pedidoClienteDAO();
        mPedidoCliente = pedidoClienteDAO.getPedidos();
    }

    public LiveData<List<PedidoCliente>> getPedidosCliente()
    {
        return mPedidoCliente;
    }

    public void insertPedidoCliente(PedidoCliente pedidoCliente)
    {
        new InsertAsyncTask(pedidoClienteDAO).execute(pedidoCliente);
    }

    public void ActualizarPedido(PedidoCliente pedidoCliente)
    {
        new ActualizarPedidoAsyncTask(pedidoClienteDAO).execute(pedidoCliente);
    }



    //Clases para manejar en segundo plano los consultas a la bd

    private class InsertAsyncTask extends AsyncTask<PedidoCliente, Void, Void>
    {
        private PedidoClienteDAO pedidoClienteDAO;

        public InsertAsyncTask(PedidoClienteDAO pedidoClienteDAO)
        {
            this.pedidoClienteDAO = pedidoClienteDAO;
        }

        @Override
        protected Void doInBackground(PedidoCliente... pedidoClientes) {

            pedidoClienteDAO.insertPedido(pedidoClientes[0]);
            return null;
        }
    }

    private class ObtenerPedidosAsyncTask extends AsyncTask<PedidoCliente, Void,Void>
    {
        private PedidoClienteDAO pedidoClienteDAO;

        public ObtenerPedidosAsyncTask(PedidoClienteDAO pedidoClienteDAO) {
            this.pedidoClienteDAO = pedidoClienteDAO;
        }

        @Override
        protected Void doInBackground(PedidoCliente... pedidoClientes) {

            pedidoClienteDAO.getPedidos();
            return null;
        }
    }

    private class ActualizarPedidoAsyncTask extends AsyncTask<PedidoCliente, Void, Void>
    {
        private PedidoClienteDAO pedidoClienteDAO;

        public ActualizarPedidoAsyncTask(PedidoClienteDAO pedidoClienteDAO) {
            this.pedidoClienteDAO = pedidoClienteDAO;
        }

        @Override
        protected Void doInBackground(PedidoCliente... pedidoClientes) {
            return null;
        }
    }


}
