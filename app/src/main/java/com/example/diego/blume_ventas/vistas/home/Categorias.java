package com.example.diego.blume_ventas.vistas.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.diego.blume_ventas.CarritoVista;
import com.example.diego.blume_ventas.R;
import com.example.diego.blume_ventas.VistaProductosComprados;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Categorias.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Categorias#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Categorias extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private LinearLayout women;
    private LinearLayout men;
    private LinearLayout accesories;
    private LinearLayout btnCarrito;
    private LinearLayout btnCompras;
    private OnFragmentInteractionListener mListener;

    public Categorias() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Categorias.
     */
    // TODO: Rename and change types and number of parameters
    public static Categorias newInstance(String param1, String param2) {
        Categorias fragment = new Categorias();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void seleccionarCategoria(Activity activity)
    {
        mListener = (OnFragmentInteractionListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categorias, container, false);

        women = view.findViewById(R.id.categoria_mujer);
        men = view.findViewById(R.id.categoria_hombre);
        accesories   = view.findViewById(R.id.categoria_accesorios);
        btnCarrito = view.findViewById(R.id.btn_carrito);
        btnCompras = view.findViewById(R.id.btn_compras);

        women.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje("Productos de mujer ");
                mListener.dataCategories("mujer");

            }
        });

        men.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje("Productos de hombre");
                mListener.dataCategories("hombre");
            }
        });

        accesories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje("Accesorios");
                mListener.dataCategories("accesorio");
            }
        });


        btnCarrito.setOnClickListener(a->{
            startActivity(new Intent(getContext(), CarritoVista.class));
        });

        btnCompras.setOnClickListener(it->{
            startActivity(new Intent(getContext(), VistaProductosComprados.class));
        });

        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public void mensaje(String msj)
    {
        Toast.makeText(getContext(), msj, Toast.LENGTH_LONG).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void dataCategories(String data);
    }
}
