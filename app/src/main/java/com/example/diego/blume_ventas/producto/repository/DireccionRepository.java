package com.example.diego.blume_ventas.producto.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.diego.blume_ventas.beans.Direccion;
import com.example.diego.blume_ventas.daos.DireccionDAO;
import com.example.diego.blume_ventas.producto.database.ProductoDatabase;

public class DireccionRepository  {

    private DireccionDAO direccionDAO;
    private LiveData<Direccion> mdireccion;

    public DireccionRepository(Application application)
    {
        ProductoDatabase db = ProductoDatabase.getDatabase(application);

        direccionDAO = db.direccionDAO();
        mdireccion = direccionDAO.getDireccion();
    }

    public LiveData<Direccion> getdireccion()
    {
        return mdireccion;
    }

    public void insertardireccion(Direccion direccion)
    {
        new InsertAsyncTask(direccionDAO).execute(direccion);

    }

    public void actualizardireccion(Direccion direccion)
    {
        new UpdateAsyncTask(direccionDAO).execute(direccion);
    }

    //Consultas a la base de datos en segundo plano

    private class InsertAsyncTask extends AsyncTask<Direccion,Void,Void>
    {
        DireccionDAO direccionDAO;

        public InsertAsyncTask(DireccionDAO direccionDAO)
        {
            this.direccionDAO=direccionDAO;
        }

        @Override
        protected Void doInBackground(Direccion... direccions) {

            direccionDAO.insertarDireccion(direccions[0]);
            return null;
        }
    }

    private class UpdateAsyncTask extends AsyncTask<Direccion, Void,Void>
    {
        DireccionDAO direccionDAO;

        public UpdateAsyncTask(DireccionDAO direccionDAO)
        {
            this.direccionDAO=direccionDAO;
        }


        @Override
        protected Void doInBackground(Direccion... direccions) {

            direccionDAO.actualizarDirrecion(direccions[0]);
            return null;
        }
    }


}
