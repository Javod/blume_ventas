package com.example.diego.blume_ventas;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.daos.ProductoDAO;
import com.example.diego.blume_ventas.databinding.ActivityProductoInformacionBinding;
import com.example.diego.blume_ventas.producto.database.ProductoDatabase;
import com.example.diego.blume_ventas.producto.viewmodel.ProductoViewModel;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

public class ProductoInformacion extends AppCompatActivity {

    private ImageView imagen;
    private Bundle paquete;
    private Producto producto;
    private Button btnCarrito;
    private ProductoViewModel productoViewModel;
    private TextView txtPrecioUno;
    private TextView txtPreciodos;
    private TextView txtPrecioTres;
    private CarouselView carouselView;
    private List<String>fotos;
    private ImageListener imageListener;
    private int CODE_PRODUC=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_informacion);
        //ActivityProductoInformacionBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_producto_informacion);

        ActivityProductoInformacionBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_producto_informacion);

        initComponents();

        paquete = new Bundle();
        producto = new Producto();

        paquete = getIntent().getExtras();

        if (paquete != null) {
            producto = (Producto) paquete.getSerializable("producto");
            binding.setProducto(producto);

            List<String> urlFotos= new ArrayList<>();

            fotos = new ArrayList<>();
            urlFotos.add(producto.getFoto1());
            urlFotos.add(producto.getFoto2());
            urlFotos.add(producto.getFoto3());

            if(producto.getPrecioOferta()>0)
            {
                txtPrecioUno.setText("S/. "+ String.format("%.2f", devolverMayor(producto.getPrecioProducto(), producto.getPrecioOferta(), producto.getPrecioOfertaDos())));

                txtPrecioUno.setPaintFlags(txtPrecioUno.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

                txtPreciodos.setText("S/. "+ String.format("%.2f",devolverMenor(producto.getPrecioProducto(), producto.getPrecioOferta(), producto.getPrecioOfertaDos())));



            }
            else {
                txtPrecioUno.setText("S/. "+ String.format("%.2f", devolverMayor(producto.getPrecioProducto(), producto.getPrecioOferta(), producto.getPrecioOfertaDos())));

                txtPreciodos.setVisibility(View.INVISIBLE);

            }




            for(String s : urlFotos)
            {
                if(!s.equals("sin foto"))
                {
                    fotos.add(s);
                }
            }

            carouselView.setPageCount(fotos.size());

            imageListener = new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    Picasso.get().load(fotos.get(position)).into(imageView);
                }
            };


            carouselView.setImageListener(imageListener);
        }



        agregarACarrito();
    }



    public double devolverMenor(double a, double b, double c)
    {
        double menor=0.0;
        menor=a;
        if(b<menor && b>0.0)
        {
            menor=b;
        }
        else if(c<menor && c>0.0)
        {
            menor=c;
        }

        return menor;
    }

    public double devolverMayor(double a, double b, double c)
    {
        double mayor=0.0;
        mayor=a;
        if(b>mayor)
        {
            mayor=b;
        }
        else if(c>mayor)
        {
            mayor=c;
        }

        return mayor;
    }

    public void initComponents() {

        btnCarrito = findViewById(R.id.btn_add_carrito);
        productoViewModel = ViewModelProviders.of(this).get(ProductoViewModel.class);
        txtPrecioUno = findViewById(R.id.txt_precio_uno);
        txtPreciodos = findViewById(R.id.txt_precio_dos);
        txtPrecioTres = findViewById(R.id.txt_precio_tres);
        carouselView = findViewById(R.id.carousel_view);
    }

    public void agregarACarrito() {

        if( producto.getStock()==0)
        {
            btnCarrito.setEnabled(false);
            btnCarrito.setText("Producto agotado");
        }
        else
        {
            btnCarrito.setOnClickListener(a -> {
                if(producto!=null)
                {
                    //Toast.makeText(ProductoInformacion.this, producto.toString(), Toast.LENGTH_LONG).show();
                    producto.setIdProducto(CODE_PRODUC);
                    agregarProducto(producto);
                    mensaje("Su producto se agrego al carrito de compras");
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                    finish();

                }
                else
                {
                    mensaje("no se pudo agregar el producto");
                }

            });
        }


    }

    public void agregarProducto(Producto producto) {

        productoViewModel.insert(producto);

    }

    public void mensaje(String msj)
    {
        Toast.makeText(this,msj, Toast.LENGTH_SHORT).show();
    }


}
