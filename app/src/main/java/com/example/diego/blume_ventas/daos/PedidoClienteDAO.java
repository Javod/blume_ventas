package com.example.diego.blume_ventas.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.diego.blume_ventas.beans.PedidoCliente;

import java.util.List;

@Dao
public interface PedidoClienteDAO {

    @Insert
    void insertPedido(PedidoCliente pedidoCliente);

    @Query("SELECT * FROM pedidocliente")
    LiveData<List<PedidoCliente>> getPedidos();

    @Update
    void actualizarPedido(PedidoCliente pedidoCliente);

}
