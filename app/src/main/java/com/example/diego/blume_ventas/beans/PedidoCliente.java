package com.example.diego.blume_ventas.beans;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = "PedidoCliente")
public class PedidoCliente implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name ="idPedido")
    private int idPedido;

    @NonNull
    @ColumnInfo(name = "codigoPedido")
    private String codigoPedido;

    @NonNull
    @ColumnInfo(name = "nombreCliente")
    private String nombreCliente;

    @NonNull
    @ColumnInfo(name = "direccionEntrega")
    private String direccionEntrega;

    @NonNull
    @ColumnInfo(name = "correoCliente")
    private String correoCliente;

    @NonNull
    @ColumnInfo(name = "fechaPedido")
    private String fechaPedido;

    @NonNull
    @ColumnInfo(name = "fechaentrega")
    private String fechaentrega;

    @NonNull
    @ColumnInfo(name = "horaEntrega")
    private String horaEntrega;

    @NonNull
    @ColumnInfo(name = "totalPago")
    private double totalPago;

    public PedidoCliente()
    {

    }

    public PedidoCliente(int idPedido, String codigoPedido, String nombreCliente, String direccionEntrega, String correoCliente, String fechaPedido, String fechaentrega, String horaEntrega, double totalPago) {
        this.idPedido = idPedido;
        this.codigoPedido = codigoPedido;
        this.nombreCliente = nombreCliente;
        this.direccionEntrega = direccionEntrega;
        this.correoCliente = correoCliente;
        this.fechaPedido = fechaPedido;
        this.fechaentrega = fechaentrega;
        this.horaEntrega = horaEntrega;
        this.totalPago=totalPago;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDireccionEntrega() {
        return direccionEntrega;
    }

    public void setDireccionEntrega(String direccionEntrega) {
        this.direccionEntrega = direccionEntrega;
    }

    public String getCorreoCliente() {
        return correoCliente;
    }

    public void setCorreoCliente(String correoCliente) {
        this.correoCliente = correoCliente;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getFechaentrega() {
        return fechaentrega;
    }

    public void setFechaentrega(String fechaentrega) {
        this.fechaentrega = fechaentrega;
    }

    public String getHoraEntrega() {
        return horaEntrega;
    }

    public void setHoraEntrega(String horaEntrega) {
        this.horaEntrega = horaEntrega;
    }

    @NonNull
    public double getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(@NonNull double totalPago) {
        this.totalPago = totalPago;
    }

    @Override
    public String toString() {
        return "PedidoCliente{" +
                "codigoPedido='" + codigoPedido + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", direccionEntrega='" + direccionEntrega + '\'' +
                ", correoCliente='" + correoCliente + '\'' +
                ", fechaPedido='" + fechaPedido + '\'' +
                ", fechaentrega='" + fechaentrega + '\'' +
                ", horaEntrega='" + horaEntrega + '\'' +
                ", totalPago=" + totalPago +
                '}';
    }
}
