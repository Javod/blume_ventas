package com.example.diego.blume_ventas.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.diego.blume_ventas.beans.Direccion;

@Dao
public interface DireccionDAO {

    @Insert
    public void insertarDireccion(Direccion direccion);

    @Query("SELECT * FROM direccion")
    LiveData<Direccion> getDireccion();

    @Update
    public void actualizarDirrecion(Direccion direccion);


}
