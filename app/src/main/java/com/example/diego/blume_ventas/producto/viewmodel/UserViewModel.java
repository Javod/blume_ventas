package com.example.diego.blume_ventas.producto.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.producto.repository.UserRepository;

public class UserViewModel extends AndroidViewModel {

    private UserRepository userRepository;
    private LiveData<User> userLiveData;


    public UserViewModel(@NonNull Application application) {
        super(application);

        userRepository = new UserRepository(application);

        userLiveData = userRepository.findUser();
    }

    public LiveData<User> getUserLiveData()
    {
        return  userLiveData;
    }

    public void insertUser(User user)
    {
        userRepository.insertUser(user);
    }

    public void updateUser(User user)
    {
        userRepository.updateuser(user);
    }
}
