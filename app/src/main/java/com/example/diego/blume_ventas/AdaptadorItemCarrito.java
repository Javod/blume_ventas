package com.example.diego.blume_ventas;

import android.app.AlertDialog;
import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.producto.viewmodel.ProductoViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorItemCarrito extends RecyclerView.Adapter<AdaptadorItemCarrito.ProductoViewHolder> implements View.OnClickListener{

    private List<Producto> productos;
    private Context context;
    private View.OnClickListener listener;
    private ProductoViewModel productoViewModel;


    public AdaptadorItemCarrito(List<Producto> productos, Context context)
    {
        this.productos= productos;
        this.context = context;

        productoViewModel = ViewModelProviders.of((FragmentActivity) context).get(ProductoViewModel.class);

    }


    @NonNull
    @Override
    public ProductoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.modelo_item_carrito, viewGroup, false);

        view.setOnClickListener(this);
        return new ProductoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoViewHolder productoViewHolder, int i) {


        Picasso.get().load(productos.get(i).getFoto1()).into(productoViewHolder.imgProducto);
        productoViewHolder.txtNombre.setText(productos.get(i).getNombreProducto());
        productoViewHolder.txtCodigo.setText("Código"+productos.get(i).getCodigoProducto());
        productoViewHolder.txtPrecio.setText("Precio: S/. "+devolverMenor(productos.get(i).getPrecioProducto(),productos.get(i).getPrecioOferta(),productos.get(i).getPrecioOfertaDos()));

        productoViewHolder.imgDelete.setOnClickListener(v->{

            mensajeDialoj(i);

        });

    }


    public void mensaje(String msj)
    {
        Toast.makeText(context, msj, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public void recargarDatos(List<Producto> nProductos)
    {
        productos  = new ArrayList<>();
        productos.addAll(nProductos);
        notifyDataSetChanged();
    }


    public List<Producto> getDataElemnts()
    {
        return productos;
    }

    public void mensajeDialoj(int position)
    {
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);

        builder.setTitle("Sacar producto del carrito");
        builder.setMessage(":(");

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                productoViewModel.delete(productos.get(position));
                productos.remove(position);
                recargarDatos(productos);

                dialog.dismiss();
                dialog.cancel();
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {

        if (listener!=null)
        {
            listener.onClick(v);
        }
    }

    public double devolverMenor(double a, double b, double c)
    {
        double menor=0.0;
        menor=a;
        if(b<menor && b>0.0)
        {
            menor=b;
        }
        else if(c<menor && c>0.0)
        {
            menor=c;
        }

        return menor;
    }

    public class ProductoViewHolder  extends RecyclerView.ViewHolder{

        private TextView txtPrecio;
        private TextView txtCodigo;
        private TextView txtNombre;
        private ImageView imgProducto;
        private ImageView imgDelete;

        public ProductoViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProducto = itemView.findViewById(R.id.img_item_carrito);
            txtNombre = itemView.findViewById(R.id.nombre_carito_producto);
            txtCodigo = itemView.findViewById(R.id.codigo_carrito_producto);
            txtPrecio = itemView.findViewById(R.id.precio_carrito_producto);
            imgDelete = itemView.findViewById(R.id.btn_eliminar_productor);

        }

    }



}
