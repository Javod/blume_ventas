package com.example.diego.blume_ventas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.producto.viewmodel.ProductoViewModel;

import java.util.ArrayList;
import java.util.List;

public class VistaProductosComprados extends AppCompatActivity {

    private RecyclerView reciclador;
    private RecyclerView.LayoutManager manager;
    private AdaptadorItemVendido itemVendido;
    private List<Producto> productosData;
    private List<Producto> productosData2;
    ProductoViewModel productoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_productos_comprados);


        initComponents();

        productoViewModel = ViewModelProviders.of(this).get(ProductoViewModel.class);

        productoViewModel.getProductos().observe(this, new Observer<List<Producto>>() {
            @Override
            public void onChanged(@Nullable List<Producto> productos) {


                for(Producto p: productos)
                {
                    if(p.getStatus().toUpperCase().equals("V"))
                    {
                        productosData2.add(p);

                    }
                }

                for(int i=0;i<productosData2.size();i++)
                {
                    productosData.add(productosData2.get(productosData2.size()-i-1));
                }




                setDatarecycler();
            }
        });
    }

    public void initComponents()
    {
        reciclador = findViewById(R.id.reciclador_v);
        productosData = new ArrayList<>();
        productosData2 = new ArrayList<>();
        manager = new LinearLayoutManager(this);

    }

    public void setDatarecycler()
    {
        reciclador.setLayoutManager(manager);
        itemVendido = new AdaptadorItemVendido(productosData, this);
        reciclador.setAdapter(itemVendido);
    }
}
