package com.example.diego.blume_ventas.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.diego.blume_ventas.beans.User;

@Dao
public interface UserDAO {

    @Insert
    void save(User user);

    @Query("SELECT * FROM user")
    LiveData<User> findUser();

    @Update
    void updateUser(User user);

}
