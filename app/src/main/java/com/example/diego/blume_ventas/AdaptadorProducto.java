package com.example.diego.blume_ventas;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.diego.blume_ventas.beans.Producto;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AdaptadorProducto extends RecyclerView.Adapter<AdaptadorProducto.ProductoViewHolder> implements View.OnClickListener {

    private List<Producto> productos;
    private View.OnClickListener listener;
    private Context context;


    public AdaptadorProducto(List<Producto> productos, Context context)
    {
        this.productos=productos;
        this.context= context;
    }

    @NonNull
    @Override
    public AdaptadorProducto.ProductoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.modelo_nuevos, viewGroup, false);

        view.setOnClickListener(this);

        return new ProductoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorProducto.ProductoViewHolder productoViewHolder, int i) {

        productoViewHolder.nombre.setText(productos.get(i).getNombreProducto());
        productoViewHolder.precio.setText("S/."+String.format("%.2f",devolverMenor(productos.get(i).getPrecioProducto(),productos.get(i).getPrecioOferta(),productos.get(i).getPrecioOfertaDos())));
        Picasso.get().load(productos.get(i).getFoto1()).into(productoViewHolder.fotoProducto);

        if(buscarPalabra(productos.get(i).getTipoProducto(),"hombre"))
        {
            productoViewHolder.fondoItem.setBackground(ContextCompat.getDrawable(context,R.drawable.borde_redondeado_hombre));
        }
        else  if (buscarPalabra(productos.get(i).getTipoProducto(),"MUJER"))
        {
            productoViewHolder.fondoItem.setBackground(ContextCompat.getDrawable(context,R.drawable.borde_redondeado_mujer));
        }
        else if(buscarPalabra(productos.get(i).getTipoProducto(),"ACCESORIO"))
        {
            productoViewHolder.fondoItem.setBackground(ContextCompat.getDrawable(context,R.drawable.borde_redondeado_accesorio));
        }
        else {
            productoViewHolder.fondoItem.setBackground(ContextCompat.getDrawable(context,R.drawable.bordes_redondeados));
        }

        if(productos.get(i).getFechaPedido().equals(fechaActual()))
        {
            productoViewHolder.txtNuevo.setVisibility(View.VISIBLE);
        }
        else
        {
            productoViewHolder.txtNuevo.setVisibility(View.INVISIBLE);
        }

    }

    public String fechaActual()
    {
        Calendar calendar = Calendar.getInstance();
        int dd= calendar.get(Calendar.DAY_OF_MONTH);
        int mm= calendar.get(Calendar.MONTH);
        int aaaa= calendar.get(Calendar.YEAR);

        mm=mm+1;
        return dd+"/"+mm+"/"+aaaa;

    }

    public void setFliter(List<Producto> resultSearch)
    {
        productos = new ArrayList<>();
        productos.addAll(resultSearch);
        notifyDataSetChanged();
    }

    public double devolverMayor(double a, double b, double c)
    {
        double mayor=0.0;
        mayor=a;
        if(b>mayor)
        {
            mayor=b;
        }
        else if(c>mayor)
        {
            mayor=c;
        }

        return mayor;
    }

    public double devolverMenor(double a, double b, double c)
    {
        double menor=0.0;
        menor=a;
        if(b<menor && b>0.0)
        {
            menor=b;
        }
        else if(c<menor && c>0.0)
        {
            menor=c;
        }

        return menor;
    }

    public  List<Producto> getDataElement()
    {
        return productos;
    }

    public boolean buscarPalabra(String texto, String categoria)
    {
        if(texto!=null)
        {
            texto=texto.toUpperCase();
            categoria=categoria.toUpperCase();
            boolean resultado=false;

            String categorias[]  = texto.split(" ");

            if(texto.contains(categoria))
            {
                return  true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }


    }



    @Override
    public int getItemCount() {
        return productos.size();
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {

        if(listener!=null)
        {
            listener.onClick(v);
        }
    }

    public class ProductoViewHolder extends RecyclerView.ViewHolder {

        private ImageView fotoProducto;
        private TextView nombre;
        private TextView precio;
        private LinearLayout fondoItem;
        private TextView txtNuevo;
        public ProductoViewHolder(@NonNull View itemView) {
            super(itemView);

            fotoProducto = itemView.findViewById(R.id.foto_producto);
            nombre = itemView.findViewById(R.id.nombre_p);
            precio = itemView.findViewById(R.id.precio_p);
            fondoItem = itemView.findViewById(R.id.fondo_item);
            txtNuevo = itemView.findViewById(R.id.txt_nuevo);

        }
    }
}
