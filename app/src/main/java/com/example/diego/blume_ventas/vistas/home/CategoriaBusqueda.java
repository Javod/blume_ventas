package com.example.diego.blume_ventas.vistas.home;

public interface CategoriaBusqueda {

    String terminoBusqueda(String termino);
}
