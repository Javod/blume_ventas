package com.example.diego.blume_ventas;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.producto.viewmodel.UserViewModel;

public class Login extends AppCompatActivity {

    private Button btnIngresar;
    private EditText txtCorreo;
    private EditText txtPassword;
    private TextView txtRegistarse;
    private UserViewModel userViewModel;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initcomponents();
        ingresar();
        registrarse();


    }


    public void initcomponents()
    {
        btnIngresar = findViewById(R.id.btn_login);
        txtCorreo = findViewById(R.id.txt_correo_usuario);
        txtPassword = findViewById(R.id.txt_password_usuario);
        txtRegistarse = findViewById(R.id.ir_registro);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        user = new User();
    }

    public void ingresar()
    {
        userViewModel.getUserLiveData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {

                if(user!=null)
                {
                    btnIngresar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(txtCorreo.getText().toString().isEmpty())
                            {
                                txtCorreo.setBackground(ContextCompat.getDrawable(Login.this,R.drawable.txt_validar_campos));
                                txtCorreo.setHint("Ingrese su correo");
                            }
                            else if(txtPassword.getText().toString().isEmpty())
                            {
                                txtPassword.setBackground(ContextCompat.getDrawable(Login.this,R.drawable.txt_validar_campos));
                                txtPassword.setHint("Ingrese contraseña");
                            }
                            else
                            {
                                if(txtCorreo.getText().toString().equals(user.getCorreo()) && txtPassword.getText().toString().equals(user.getPassword()))
                                {
                                    startActivity(new Intent(Login.this, MainActivity.class));

                                }
                                else
                                {
                                    Toast.makeText(Login.this,"Erro al ingresar su correo o contraseña", Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    });
                }
                else
                {
                    Toast.makeText(Login.this,"Debe registrarse para ingresar", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    public void registrarse()
    {
        txtRegistarse.setOnClickListener(v->{

            startActivity(new Intent(Login.this, FormularioRegistro.class));
        });
    }
}
