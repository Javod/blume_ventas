package com.example.diego.blume_ventas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.producto.viewmodel.UserViewModel;

public class SplashBlume extends AppCompatActivity {

    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_blume);

        initComponets();




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                userViewModel.getUserLiveData().observe(SplashBlume.this, new Observer<User>() {
                    @Override
                    public void onChanged(@Nullable User user) {
                        if(user!=null)
                        {
                            startActivity(new Intent(SplashBlume.this, MainActivity.class));
                            overridePendingTransition(R.anim.fade_out,R.anim.fade_in);
                            finish();
                        }
                        else
                        {
                            startActivity(new Intent(SplashBlume.this, Login.class));
                            overridePendingTransition(R.anim.fade_out,R.anim.fade_in);
                            finish();
                        }
                    }
                });


            }
        }, 2300);

    }


    public void initComponets()
    {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }
}
