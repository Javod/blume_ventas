package com.example.diego.blume_ventas.beans;

import java.io.Serializable;

public class DetalleProductoPedido implements Serializable {

    private int idDetalle;
    private String codigoPedido;
    private String codigoProducto;

    public DetalleProductoPedido()
    {

    }

    public DetalleProductoPedido(int idDetalle, String codigoPedido, String codigoProducto) {
        this.idDetalle = idDetalle;
        this.codigoPedido = codigoPedido;
        this.codigoProducto = codigoProducto;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }
}
