package com.example.diego.blume_ventas.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.diego.blume_ventas.beans.Producto;

import java.util.List;

@Dao
public interface ProductoDAO {

    @Insert
    void insert(Producto producto);


    @Query("SELECT * FROM producto")
    LiveData<List<Producto>> findAll();

    @Update
    void updateProducto(Producto producto);

    @Query("DELETE FROM producto")
    void deleteAll();

    @Delete
    void deleteProducto(Producto...productos);


}
