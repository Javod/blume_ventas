package com.example.diego.blume_ventas.beans;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = "producto")
public class Producto implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "idProducto")
    private int idProducto;

    @NonNull
    @ColumnInfo(name = "codigoProducto")
    private String codigoProducto;

    @NonNull
    @ColumnInfo(name = "nombreProducto")
    private String nombreProducto;

    @NonNull
    @ColumnInfo(name = "precioProducto")
    private double precioProducto;

    @NonNull
    @ColumnInfo(name = "precioOferta")
    private double precioOferta;

    @NonNull
    @ColumnInfo(name = "precioOfertaDos")
    private double precioOfertaDos;


    @NonNull
    @ColumnInfo(name = "tipoProducto")
    private String tipoProducto;

    @NonNull
    @ColumnInfo(name = "colorProducto")
    private String colorProducto;

    @NonNull
    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @NonNull
    @ColumnInfo(name = "foto1")
    private String foto1;


    @ColumnInfo(name = "foto2")
    private String foto2;


    @ColumnInfo(name = "foto3")
    private String foto3;

    @NonNull
    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name="fechaPedido")
    private String fechaPedido;

    @ColumnInfo(name="codigoPedido")
    private String codigoPedido;

    @ColumnInfo(name = "stock")
    private int stock;

    public Producto()
    {

    }

    public Producto(@NonNull int idProducto, @NonNull String codigoProducto, @NonNull String nombreProducto, @NonNull double precioProducto, @NonNull double precioOferta, @NonNull double precioOfertaDos, @NonNull String tipoProducto, @NonNull String colorProducto, @NonNull String descripcion, @NonNull String foto1, String foto2, String foto3, @NonNull String status, String fechaPedido, String codigoPedido, int stock) {
        this.idProducto = idProducto;
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.precioProducto = precioProducto;
        this.precioOferta = precioOferta;
        this.precioOfertaDos = precioOfertaDos;
        this.tipoProducto = tipoProducto;
        this.colorProducto = colorProducto;
        this.descripcion = descripcion;
        this.foto1 = foto1;
        this.foto2 = foto2;
        this.foto3 = foto3;
        this.status = status;
        this.fechaPedido = fechaPedido;
        this.codigoPedido = codigoPedido;
        this.stock = stock;
    }

    @NonNull
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(@NonNull int idProducto) {
        this.idProducto = idProducto;
    }

    @NonNull
    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(@NonNull String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @NonNull
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(@NonNull String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    @NonNull
    public double getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(@NonNull double precioProducto) {
        this.precioProducto = precioProducto;
    }

    @NonNull
    public double getPrecioOferta() {
        return precioOferta;
    }

    public void setPrecioOferta(@NonNull double precioOferta) {
        this.precioOferta = precioOferta;
    }

    @NonNull
    public double getPrecioOfertaDos() {
        return precioOfertaDos;
    }

    public void setPrecioOfertaDos(@NonNull double precioOfertaDos) {
        this.precioOfertaDos = precioOfertaDos;
    }

    @NonNull
    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(@NonNull String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    @NonNull
    public String getColorProducto() {
        return colorProducto;
    }

    public void setColorProducto(@NonNull String colorProducto) {
        this.colorProducto = colorProducto;
    }

    @NonNull
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(@NonNull String descripcion) {
        this.descripcion = descripcion;
    }

    @NonNull
    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(@NonNull String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    @NonNull
    public String getStatus() {
        return status;
    }

    public void setStatus(@NonNull String status) {
        this.status = status;
    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "idProducto=" + idProducto +
                ", codigoProducto='" + codigoProducto + '\'' +
                ", nombreProducto='" + nombreProducto + '\'' +
                ", precioProducto=" + precioProducto +
                ", precioOferta=" + precioOferta +
                ", precioOfertaDos=" + precioOfertaDos +
                ", tipoProducto='" + tipoProducto + '\'' +
                ", colorProducto='" + colorProducto + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", foto1='" + foto1 + '\'' +
                ", foto2='" + foto2 + '\'' +
                ", foto3='" + foto3 + '\'' +
                ", status='" + status + '\'' +
                ", fechaPedido='" + fechaPedido + '\'' +
                ", codigoPedido='" + codigoPedido + '\'' +
                ", stock=" + stock +
                '}';
    }
}
