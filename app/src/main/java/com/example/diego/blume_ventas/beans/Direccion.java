package com.example.diego.blume_ventas.beans;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "direccion")
public class Direccion implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idDireccion")
    private int idDireccion;

    @ColumnInfo(name = "direccioCliente")
    private String direccioCliente;

    public Direccion()
    {

    }

    public Direccion(int idDireccion,String direccioCliente)
    {
        this.idDireccion=idDireccion;
        this.direccioCliente=direccioCliente;
    }

    public String getDireccioCliente()
    {
        return this.direccioCliente;
    }

    public void setDireccioCliente(String direccioCliente) {
        this.direccioCliente = direccioCliente;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }
}
