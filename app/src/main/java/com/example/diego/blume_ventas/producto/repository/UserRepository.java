package com.example.diego.blume_ventas.producto.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.daos.UserDAO;
import com.example.diego.blume_ventas.producto.database.ProductoDatabase;

public class UserRepository {

    private UserDAO userDAO;
    private LiveData<User> userLiveData;

    public UserRepository(Application application)
    {
        ProductoDatabase productoDatabase = ProductoDatabase.getDatabase(application);

        userDAO = productoDatabase.userDAO();

        userLiveData = userDAO.findUser();
    }

    public LiveData<User> findUser()
    {
        return userLiveData;
    }

    public void insertUser(User user)
    {
        new InsertUserasyncTask(userDAO).execute(user);
    }

    public void updateuser(User user)
    {
        new UpdateUserAsyncTask(userDAO).execute(user);
    }


    //Consultas a la base de datos en segundo plano
    //---------------------------------------------


    private class  InsertUserasyncTask extends AsyncTask<User,Void,Void>
    {
        private UserDAO userDAO;

        public InsertUserasyncTask(UserDAO userDAO) {
            this.userDAO = userDAO;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDAO.save(users[0]);


            return null;
        }
    }

    private class UpdateUserAsyncTask extends AsyncTask<User,Void,Void>
    {
        private UserDAO userDAO;

        public UpdateUserAsyncTask(UserDAO userDAO) {
            this.userDAO = userDAO;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDAO.updateUser(users[0]);
            return null;
        }
    }
}
