package com.example.diego.blume_ventas;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.producto.viewmodel.PedidoClienteViewModel;

import java.util.List;

public class AdaptadorItemFactura extends RecyclerView.Adapter<AdaptadorItemFactura.PedidoClienteViewHolder> {

    private List<PedidoCliente> pedidosFactura;
    private Context context;
    private PedidoClienteViewModel pedidoClienteViewModel;

    public AdaptadorItemFactura(List<PedidoCliente> pedidosFactura, Context context)
    {
        this.pedidosFactura = pedidosFactura;
        this.context = context;
        pedidoClienteViewModel= ViewModelProviders.of((FragmentActivity) context).get(PedidoClienteViewModel.class);
    }

    @NonNull
    @Override
    public PedidoClienteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.modelo_factura, viewGroup, false);
        return new PedidoClienteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PedidoClienteViewHolder pedidoClienteViewHolder, int i) {

        pedidoClienteViewHolder.txtNombreF.setText(pedidosFactura.get(i).getNombreCliente());
        pedidoClienteViewHolder.txtCodigoF.setText(pedidosFactura.get(i).getCodigoPedido());
        pedidoClienteViewHolder.txtDirecF.setText(pedidosFactura.get(i).getDireccionEntrega());
        pedidoClienteViewHolder.txtCorreoF.setText(pedidosFactura.get(i).getCorreoCliente());
        pedidoClienteViewHolder.txtFechaPedidoF.setText(pedidosFactura.get(i).getFechaPedido());
        pedidoClienteViewHolder.txtFechaEntregaF.setText(pedidosFactura.get(i).getFechaentrega());
        pedidoClienteViewHolder.txtHoraentregaF.setText(pedidosFactura.get(i).getHoraEntrega());
        pedidoClienteViewHolder.txtTotalF.setText(String.valueOf(pedidosFactura.get(i).getTotalPago()));


    }

    @Override
    public int getItemCount() {
        return pedidosFactura.size();
    }

    public class PedidoClienteViewHolder extends  RecyclerView.ViewHolder{

        TextView txtNombreF;
        TextView txtCodigoF;
        TextView txtDirecF;
        TextView txtCorreoF;
        TextView txtFechaPedidoF;
        TextView txtFechaEntregaF;
        TextView txtHoraentregaF;
        TextView txtTotalF;
        public PedidoClienteViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNombreF = itemView.findViewById(R.id.txt_nombre_f);
            txtCodigoF = itemView.findViewById(R.id.txt_codigo_f);
            txtDirecF  = itemView.findViewById(R.id.txt_direc_f);
            txtCorreoF = itemView.findViewById(R.id.txt_correo_f);
            txtFechaPedidoF = itemView.findViewById(R.id.txt_fecha_pedido_f);
            txtFechaEntregaF = itemView.findViewById(R.id.txt_fecha_entrega_f);
            txtHoraentregaF = itemView.findViewById(R.id.txt_hora_entrega_f);
            txtTotalF  = itemView.findViewById(R.id.txt_total_f);

        }
    }
}
