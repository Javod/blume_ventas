package com.example.diego.blume_ventas.producto.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.diego.blume_ventas.beans.Direccion;
import com.example.diego.blume_ventas.producto.repository.DireccionRepository;

public class DireccionViewModel extends AndroidViewModel {

    private DireccionRepository direccionRepository;
    private LiveData<Direccion> direccionLiveData;

    public DireccionViewModel(@NonNull Application application) {
        super(application);

        direccionRepository = new DireccionRepository(application);
        direccionLiveData = direccionRepository.getdireccion();
    }

    public LiveData<Direccion> getDireccionLiveData()
    {
        return direccionLiveData;
    }

    public void insertDireccion(Direccion direccion)
    {
        direccionRepository.insertardireccion(direccion);
    }

    public  void actualizarDireccion(Direccion direccion)
    {
        direccionRepository.actualizardireccion(direccion);
    }


}
