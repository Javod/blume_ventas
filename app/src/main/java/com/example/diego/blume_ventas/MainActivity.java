package com.example.diego.blume_ventas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.conexion.rest.RestCliente;
import com.example.diego.blume_ventas.producto.viewmodel.UserViewModel;
import com.example.diego.blume_ventas.vistas.home.Categorias;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Categorias.OnFragmentInteractionListener, PedidoRegistro.OnFragmentInteractionListener {

    private List<Producto> productos;
    private List<Producto> searchResult;
    private RecyclerView reciclador;
    private AdaptadorProducto adaptadorProducto;
    private RecyclerView.LayoutManager layoutManager;
    private Categorias categorias;
    private boolean bandera=false;
    private PedidoRegistro pedidoRegistro;
    private boolean banderaFactura=false;
    private FrameLayout fragmentoFactura;
    private ImageView imgGeneroLogo;
    private TextView nombreUsuarioMenu;
    private TextView correoUsuarioMenu;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        nombreUsuarioMenu = header.findViewById(R.id.txt_nombre_usuario_menu);
        correoUsuarioMenu = header.findViewById(R.id.txt_correo_usuario_menu);
        imgGeneroLogo = header.findViewById(R.id.logo_genero);


        getDataJson();


        initcomponents();
        asignarfragmentoCategorias();
        //categorias.seleccionarCategoria(this);
        //llenarDatos();


        //irDetalleProducto();
        irDetalleProducto2();

        getSupportFragmentManager().beginTransaction().add(R.id.fragmento_facturas, pedidoRegistro).commit();

        mostrarDatosDeUsuarioEnMenu();


    }

    public void asignarfragmentoCategorias()
    {
        getSupportFragmentManager().beginTransaction().add(R.id.categorias, categorias).commit();
    }

    private  void  initcomponents()
    {
        productos = new ArrayList<>();
        reciclador = findViewById(R.id.reciclador);
        layoutManager = new GridLayoutManager(this,2);
        reciclador.setLayoutManager(layoutManager);
        adaptadorProducto = new AdaptadorProducto(productos, getApplicationContext());
        reciclador.setAdapter(adaptadorProducto);
        categorias = new Categorias();
        pedidoRegistro = new PedidoRegistro();
        fragmentoFactura = findViewById(R.id.fragmento_facturas);
        userViewModel = ViewModelProviders.of(MainActivity.this).get(UserViewModel.class);

    }


    private void mostrarDatosDeUsuarioEnMenu()
    {
        userViewModel.getUserLiveData().observe(MainActivity.this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {

                if(user!=null)
                {
                    nombreUsuarioMenu.setText(user.getNombreUsu());
                    correoUsuarioMenu.setText(user.getCorreo());

                    if(user.getGeneroUsu().toUpperCase().equals("HOMBRE".toUpperCase()))
                    {
                        imgGeneroLogo.setImageResource(R.drawable.icon_hombre);
                    }
                    else if(user.getGeneroUsu().toUpperCase().equals("MUJER".toUpperCase()))
                    {
                        imgGeneroLogo.setImageResource(R.drawable.icon_mujer);
                    }
                }

            }
        });




    }

    public void irDetalleProducto()
    {

        adaptadorProducto.setOnClickListener(a->{
            Producto p = productos.get(reciclador.getChildAdapterPosition(a));
            Bundle paquete = new Bundle();
            paquete.putSerializable("producto", p);
            Intent intent = new Intent(this, ProductoInformacion.class);
            intent.putExtras(paquete);
            startActivity(intent);
        });
    }

    public void irDetalleProducto2()
    {

        adaptadorProducto.setOnClickListener(a->{
            Producto p = adaptadorProducto.getDataElement().get(reciclador.getChildAdapterPosition(a));
            Bundle paquete = new Bundle();
            paquete.putSerializable("producto", p);
            Intent intent = new Intent(this, ProductoInformacion.class);
            intent.putExtras(paquete);
            //Toast.makeText(MainActivity.this, p.toString(), Toast.LENGTH_LONG).show();
            startActivity(intent);
        });
    }



    private void llenarDatos() {

        productos.add(new Producto(0,"a1111","Zapato Negro", 25.99,18.90,0.0,"Zapato Negro de mujer ","negro","Zapato negro de charol", "https://ae01.alicdn.com/kf/HTB1ljNjSVXXXXbSaXXXq6xXFXXXk/Zapatos-Mujer-tacones-altos-Metal-decoraci-n-plataforma-mujeres-bombas-sandalias-boca-de-pescado-Zapatos-plataforma.jpg_640x640.jpg","sin foto","sin foto","n","28/2/2019","@correo",1));
        productos.add(new Producto(0,"a2311","Zapato Rojo", 24.99,20.90,0.0,"calzado mujer","rojo","Zapato rojo de cuero taco 9", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBBEDxZyegT3J13OuInGgg6F80PQn1O_zZafXGEHkv-zigWpXM","https://mlstaticquic-a.akamaihd.net/zapatos-taco-alto-D_NQ_NP_656701-MLU20384358104_082015-F.jpg","sin foto","n","28/2/2019","@correo",2));
        productos.add(new Producto(0,"a1111","Zapato Rosado", 38.99,18.90,0.0," mujer calzado","rosado","Zapato rosado con platadorma", "https://static.wixstatic.com/media/980d8a_495b18aa243e41e788eb723d50b6a731~mv2.jpg/v1/fill/w_498,h_498,al_c,q_90/file.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",3));
        productos.add(new Producto(0,"a15611","Reloj Rolex", 24.99,12.90,0.0,"hombre","negro y rojo platino","Zapato rosado con platadorma", "https://elma.dreivip.com/img/imgProduc/mr_7761/c/77610028_1.jpg","https://images-na.ssl-images-amazon.com/images/I/514Df4bBv0L.jpg","https://www.torresjoyeria.com/3290-thickbox_default/reloj-hombre-jaguar-j663-2.jpg","n","28/2/2019","@correo",1));
        productos.add(new Producto(0,"a1111","Reloj Seiko", 32.99,0.0,0.0,"reloj hombre","Plata y marron","reloj plateado con correa de cuero", "https://ae01.alicdn.com/kf/HTB1p_tlIVXXXXb2XpXXq6xXFXXXc/Caliente-reloj-de-hombre-reloj-hombre-mejores-hombres-de-lujo-marca-moda-reloj-de-cuarzo-correa.jpg","sin foto","sin foto","n","28/02/2019","@correo",2));
        productos.add(new Producto(0,"a1111","Pulcera", 8.50,0.0,0.0,"pulcera  esclava hombre","negro y marron","reloj plateado con correa de cuero", "https://alfileresyregalosdenovia.com/59660-large_default/pulsera-negra-hombre-.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a11541","Pulcera de Oro", 16.90,0.0,0.0,"pulcera  esclava hombre","dorado","Pulcera de oro", "http://www.brazaletesysortijas.com/wp-content/uploads/2016/06/pulsera-hombre-oro-n943.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",1));
        productos.add(new Producto(0,"a1111","Lentes de sol", 22.90,0.0,0.0,"Lentes para hombre","negro y marfil","Pulcera de oro", "https://images-na.ssl-images-amazon.com/images/I/41stKzrP%2BdL._SX425_.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",2));
        productos.add(new Producto(0,"a11121","Lentes de sol", 26.90,20.90,0.0,"Lentes para hombre","negro","Pulcera de oro", "https://images-na.ssl-images-amazon.com/images/I/41stKzrP%2BdL._SX425_.jpghttps://i.linio.com/p/e044db6894380805a2620a902a5f01b5-product.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",2));
        productos.add(new Producto(0,"a1111","Lentes de sol", 22.90,0.0,0.0,"Lentes para hombre","negro","Pulcera de oro", "https://i.blogs.es/575cb3/08-110120-0001-01_a2/450_1000.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",2));
        productos.add(new Producto(0,"a6711","Reloj de mujer", 38.90,0.0,0.0,"reloj mujer","rosado","reloj rosado de mujer con oro", "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA03/201710/18/00110660005298____1__516x640.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",3));
        productos.add(new Producto(0,"a1111","Pendientes", 42.39,40.90,0.0,"pendientes de mujer","dorado y plata ","pendientes de mujer de oro, plata y brillantes", "http://www.aquihayunchollo.es/images/category_4/2018%20Nuevo%20Estilo%20AnaZoz%20Pendientes%20Mujer%20Pendientes%20Chapado%20en%20Oro%20Mujer%20Pendientes%20Estrella%20Circonita%20Blanco%20Pendientes%20Oro%200220978785.jpg","","","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a777","Protector ", 16,0.0,0.0,"accesorio Protector para samgsum de madera","marfil","protector de madera", "https://http2.mlstatic.com/D_NP_772886-MLM27951605952_082018-Q.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",2));
        productos.add(new Producto(0,"a1111","Protector mujer", 16,0.0,0.0,"accesorio Protector para samgsum con reverso liquido y colores","rosado","protector de madera", "https://endimages.s3.amazonaws.com/news/fab3f6828f0711e5aad90eb04a1bba78.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a14e41","Audífonos negro ", 29.90,0.0,0.0,"accesorio Audífonos negros bluetooth con estuche","negro","Audífonos negros bluetooth con estuche", "https://images-na.ssl-images-amazon.com/images/I/61tAQCKS%2BXL._SX425_.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a1111","Audífonos negros con mp3", 45.90,0.0,0.0,"accesorio Audífonos negros con mp3","negro","Audífonos negros bluetooth con estuche", "https://www.steren.com.mx/media/product/19538bd0e/audifonos-bluetooth-con-reproductor-mp3.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",5));
        productos.add(new Producto(0,"a10o1","Audífonos Philips", 11.90,0.0,0.0,"accesorio Audífonos Philips bluetooth ","turquesa morado","Audífonos negros clasicos", "https://www.pcfactory.cl/public/foto/29057/3_500.jpg?t=1523624006","sin foto","sin foto","n","dd/mm/aaa","@correo",5));
        productos.add(new Producto(0,"a1111","Reloj de mujer", 36.90,0.0,0.0,"Reloj de mujer blanco de oro y cuero","blanco dorado","Reloj de mujer", "https://ae01.alicdn.com/kf/HTB15GArb_qWBKNjSZFAq6ynSpXaj/Relojes-de-moda-para-Mujer-Reloj-2017-dos-vueltas-pulsera-de-heridas-Reloj-de-cuarzo-para.jpg_640x640.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a77uy1","Reloj de mujer ", 29.90,0.0,0.0,"Reloj de mujer blanco de oro y cuero Quartz","negro","Reloj de mujer", "http://cdn.shopify.com/s/files/1/1778/1889/products/product-image-76526672_grande.jpg?v=1489454762","sin foto","sin foto","n","dd/mm/aaa","@correo",4));
        productos.add(new Producto(0,"a0pp11","Casaca para hombre ", 59.90,34.90,0.0,"Casaca para hombre marrón","negro","Reloj de mujer", "https://home.ripley.com.pe/Attachment/WOP_5/2016194703595/2016194703595_2.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",5));
        productos.add(new Producto(0,"a0pp11","Pack de Audífonos Ewtto ", 12.90,0.0,0.0,"accesorio audifonos hombre  mujer","negro azul","Reloj de mujer", "https://http2.mlstatic.com/audifonos-ewtto-hands-free-manos-libres-et-a1137m-D_NQ_NP_722243-MPE26544788817_122017-F.jpg","https://i.linio.com/p/13ce3eb1d3f70d5cebefa845656f114a-product.jpg","sin foto","n","dd/mm/aaa","@correo",5));
        productos.add(new Producto(0,"a0pp11","Pendientes alas de angel", 5.90,0.0,0.0,"pendientes aretes mujer","rosado  transparente","Pendientes alas de angel", "https://www.eljardindeldeseo.es/34829-home_default/pendientes-mujer-originales-rosas.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",6));
        productos.add(new Producto(0,"a0pp11","Labial rosado Mate N° 33", 8.90,0.0,0.0,"labial mujer","rosado  mate","Labial ", "https://images-na.ssl-images-amazon.com/images/I/61NUPc33hIL._SX425_.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",5));
        productos.add(new Producto(0,"a0pp11","Labial marrón Mate N° 23", 8.90,0.0,0.0,"labial mujer","marrón  mate","Labial", "https://images-na.ssl-images-amazon.com/images/I/41KCJlOO37L.jpg","sin foto","sin foto","n","dd/mm/aaa","@correo",4));

    }

    public void getDataJson()
    {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.1.60:8080/BlumeServicioRestVentas/rest.envio.productos.pedidos.v1/").addConverterFactory(GsonConverterFactory.create()).build();

        RestCliente restCliente = retrofit.create(RestCliente.class);

        Call<List<Producto>> call = restCliente.getProductos();

        call.enqueue(new Callback<List<Producto>>() {
            @Override
            public void onResponse(Call<List<Producto>> call, Response<List<Producto>> response) {

                String f=" ";

                for(Producto p : response.body())
                {
                    productos.add(p);
                }


                adaptadorProducto.notifyDataSetChanged();

            }



            @Override
            public void onFailure(Call<List<Producto>> call, Throwable t) {

                Toast.makeText(MainActivity.this,"no se realizo la conexion",Toast.LENGTH_SHORT).show();

                llenarDatos();
                adaptadorProducto.notifyDataSetChanged();

            }
        });


    }

    private List getSearch(String text)
    {
        text.toLowerCase();
        searchResult = new ArrayList<>();

        for(Producto p : productos)
        {
            String palabras = p.getTipoProducto().toLowerCase();
            if(palabras.contains(text))
            {
                searchResult.add(p);
            }
        }

        return searchResult;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(banderaFactura)
        {
            fragmentoFactura.setVisibility(View.GONE);
            reciclador.setVisibility(View.VISIBLE);
            banderaFactura=false;
        }
        else if(bandera)
        {
            List<Producto> searchResult =getSearch(" ");
            adaptadorProducto.setFliter(searchResult);
            bandera=false;
        }
        else
        {
            super.onBackPressed();
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem buscarItem = menu.findItem(R.id.action_search);
        final SearchView buscar = (SearchView) buscarItem.getActionView();

        buscar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                List<Producto> searchResult =getSearch(s);
                adaptadorProducto.setFliter(searchResult);


                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.facturas) {
            // Handle the camera action
            reciclador.setVisibility(View.GONE);
            fragmentoFactura.setVisibility(View.VISIBLE);
            banderaFactura=true;

        } else if (id == R.id.nav_gallery) {

            fragmentoFactura.setVisibility(View.GONE);
            reciclador.setVisibility(View.VISIBLE);


        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_edit_datos) {
            startActivity(new Intent(MainActivity.this,FormularioRegistro.class));

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

   @Override
   public void dataCategories(String data)
   {
       List<Producto> searchResult =getSearch(data);
       adaptadorProducto.setFliter(searchResult);
       bandera=true;
   }

}
