package com.example.diego.blume_ventas.producto.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.producto.repository.ProductoRepository;

import java.util.List;

public class ProductoViewModel extends AndroidViewModel {
    private ProductoRepository productoRepository;
    private LiveData<List<Producto>> mAllProductos;


    public ProductoViewModel(@NonNull Application application) {
        super(application);

        productoRepository = new ProductoRepository(application);
        mAllProductos = productoRepository.getAllProductos();
    }

    public LiveData<List<Producto>> getProductos()
    {
        return mAllProductos;
    }

    public void insert(Producto producto)
    {
        productoRepository.insertProducto(producto);
    }

    public void updateProducto(Producto producto)
    {
        productoRepository.updateProducto(producto);
    }

    public void delete(Producto producto)
    {
        productoRepository.deleteProducto(producto);
    }

    public void deleteall()
    {
        productoRepository.deleteAllProductos();
    }
}
