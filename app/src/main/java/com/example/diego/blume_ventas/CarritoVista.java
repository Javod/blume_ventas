package com.example.diego.blume_ventas;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.DetalleProductoPedido;
import com.example.diego.blume_ventas.beans.Direccion;
import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.conexion.rest.RestCliente;
import com.example.diego.blume_ventas.producto.viewmodel.DireccionViewModel;
import com.example.diego.blume_ventas.producto.viewmodel.PedidoClienteViewModel;
import com.example.diego.blume_ventas.producto.viewmodel.ProductoViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CarritoVista extends AppCompatActivity {

    private TextView txtTotalCarrito;
    ProductoViewModel productoViewModel;
    String text = null;
    private double totalPrecio=0.0;
    private double totalPago=0.0;

    private RecyclerView reciclador;
    private RecyclerView.LayoutManager manager;
    private AdaptadorItemCarrito adaptadorItemCarrito;
    private List<Producto> productosData;

    private TextView txtPrecioTotal;
    private TextView txtCantidad;
    private Button btnRealizarPedido;
    private EditText txtDireccion;
    private LinearLayout formularioPedido;
    private TextView fechaEntrega;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Button btnHacerPedido;
    private int ddE;
    private int mmE;
    private int aaaaE;
    private TextView txtHoraEntrega;
    private int HH;
    private int MM;
    private String horario;
    private TimePickerDialog.OnTimeSetListener timeSetListener;
    private Direccion direccion;
    private PedidoCliente pedidoCliente;
    private Button btnUltimaDireccion;
    DireccionViewModel direccionViewModel;
    PedidoClienteViewModel pedidoClienteViewModel;
    private String nombreUsuario="Diego Rodriguez";
    private String correo="mark.sergen@gmail.com";
    private boolean banderaDirec=false;
    private LinearLayout carritoVacio;

    private List<DetalleProductoPedido> pedidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito_final);

        initcomponents();


        productoViewModel = ViewModelProviders.of(this).get(ProductoViewModel.class);
        direccionViewModel = ViewModelProviders.of(this).get(DireccionViewModel.class);
        pedidoClienteViewModel = ViewModelProviders.of(this).get(PedidoClienteViewModel.class);

        productoViewModel.getProductos().observe(this, new Observer<List<Producto>>() {
            @Override
            public void onChanged(@Nullable List<Producto> productos) {
                productosData = new ArrayList<>();

                for(Producto p :productos)
                {
                    if(p.getStatus().toUpperCase().equals("N"))
                    {
                        productosData.add(p);
                    }
                }

                if(productosData!=null)
                {
                    setDataRecycler();

                    for(int i=0;i<adaptadorItemCarrito.getDataElemnts().size(); i++)
                    {
                        totalPrecio = totalPrecio + devolverMenor(adaptadorItemCarrito.getDataElemnts().get(i).getPrecioProducto(),adaptadorItemCarrito.getDataElemnts().get(i).getPrecioOferta(),adaptadorItemCarrito.getDataElemnts().get(i).getPrecioOfertaDos());
                    }

                    txtTotalCarrito.setText("Total: S/. "+String.format("%.2f", totalPrecio));
                    txtPrecioTotal.setText("S/. "+String.format("%.2f", totalPrecio));
                    txtCantidad.setText(" "+productosData.size());
                    totalPago = totalPrecio;

                    if(totalPago==0)
                    {
                        reciclador.setVisibility(View.GONE);
                        carritoVacio.setVisibility(View.VISIBLE);
                    }
                    totalPrecio=0;
                }




            }
        });


        direccionViewModel.getDireccionLiveData().observe(this, new Observer<Direccion>() {
            @Override
            public void onChanged(@Nullable Direccion direccion) {
                if(direccion==null)
                {
                    btnUltimaDireccion.setEnabled(false);
                    banderaDirec=false;
                }
                else
                {
                    banderaDirec=true;
                    btnUltimaDireccion.setOnClickListener(v->{

                        txtDireccion.setText(direccion.getDireccioCliente());

                    });
                }


            }
        });

        btnRealizarPedido.setOnClickListener(v->{

            formularioPedido.setVisibility(View.VISIBLE);
            btnRealizarPedido.setVisibility(View.GONE);


        });

        seleccionarFechaEntrega();
        seleccionarHoraEntrega();

        btnHacerPedido.setOnClickListener(v->{


           if(productosData.size()==0)
           {
               mensaje("Aun no hay productos en tu carrito");
           }
           else
           {
               if(txtDireccion.getText().toString().isEmpty())
               {
                   txtDireccion.setBackground(ContextCompat.getDrawable(this,R.drawable.txt_borde_validation));
                   txtDireccion.setHint("Ingrese una direccion por favor :)");
               }
               else  if (fechaEntrega.getText().toString().isEmpty())
               {
                   fechaEntrega.setBackground(ContextCompat.getDrawable(this,R.drawable.txt_borde_validation));
                   fechaEntrega.setHint("Ingrese la fecha de entrega");
               }
               else if(txtHoraEntrega.getText().toString().isEmpty())
               {
                   txtHoraEntrega.setBackground(ContextCompat.getDrawable(this,R.drawable.txt_borde_validation));
                   txtHoraEntrega.setHint("Ingrese una hora de entrega");
               }
               else
               {
                   formularioPedido.setVisibility(View.GONE);
                   btnRealizarPedido.setVisibility(View.VISIBLE);

                   for(Producto p :productosData)
                   {
                       p.setStatus("V");

                   }


                   for(Producto p: productosData)
                   {
                       productoViewModel.updateProducto(p);

                   }

                   for(int i=0;i<productosData.size(); i++)
                   {
                       pedidos.add(new DetalleProductoPedido(1,correo+asignarFecha(),productosData.get(i).getCodigoProducto()));
                       enviarDetallePedidoProducto(new DetalleProductoPedido(1,correo+asignarFecha(),productosData.get(i).getCodigoProducto()));
                   }

                   direccion.setDireccioCliente(txtDireccion.getText().toString());

                   if(banderaDirec==false)
                   {
                       direccionViewModel.insertDireccion(direccion);
                   }
                   else
                   {
                       direccionViewModel.actualizarDireccion(direccion);
                   }



                  // asignarDatosPedido(pedidoCliente);

                   pedidoCliente.setCodigoPedido(correo+asignarFecha());
                   pedidoCliente.setCorreoCliente(correo);
                   pedidoCliente.setFechaPedido(asignarFecha());
                   pedidoCliente.setDireccionEntrega(txtDireccion.getText().toString());
                   pedidoCliente.setHoraEntrega(txtHoraEntrega.getText().toString());
                   pedidoCliente.setNombreCliente(nombreUsuario);
                   pedidoCliente.setTotalPago(totalPago);
                   pedidoCliente.setFechaentrega(fechaEntrega.getText().toString());

                   pedidoClienteViewModel.insertPedido(pedidoCliente);
                   enviarPedido(pedidoCliente);


                   Intent i = new Intent(this, MainActivity.class);
                   startActivity(i);
                   finish();

                   mensaje("Su Pedido se ha enviado, gracias por su compra");

               }
           }


        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                ddE=dayOfMonth;
                mmE = month+1;
                aaaaE= year;

                fechaEntrega.setText(ddE+"/"+mmE+"/"+aaaaE);
            }
        };

        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                horario = (hourOfDay<12)?"AM":"PM";

                txtHoraEntrega.setText(hourOfDay+":"+minute+" "+horario);

            }
        };

    }

    public void initcomponents()
    {
        txtPrecioTotal = findViewById(R.id.txt_precio_total);
        txtCantidad = findViewById(R.id.txt_cantidad);
        txtTotalCarrito = findViewById(R.id.txt_total_carrito);
        reciclador = findViewById(R.id.reciclador);
        productosData = new ArrayList<>();
        manager = new LinearLayoutManager(this);
        btnRealizarPedido = findViewById(R.id.btn_realizar_pedido);
        txtDireccion = findViewById(R.id.txt_direccion);
        formularioPedido = findViewById(R.id.formulario_pedido);
        fechaEntrega = findViewById(R.id.fecha_entrega);
        btnHacerPedido = findViewById(R.id.btn_hacer_pedido);
        txtHoraEntrega = findViewById(R.id.txt_hora_entrega);
        direccion = new Direccion();
        pedidoCliente = new PedidoCliente();
        btnUltimaDireccion = findViewById(R.id.btn_ultima_dirrecion);
        pedidos = new ArrayList<>();
        carritoVacio = findViewById(R.id.carrito_vacio);
    }

    public String asignarFecha()
    {

        Calendar calendar = Calendar.getInstance();
        int dd= calendar.get(Calendar.DAY_OF_MONTH);
        int mm= calendar.get(Calendar.MONTH)+1;
        int aaaa= calendar.get(Calendar.YEAR);

        return " "+dd+"/"+mm+"/"+aaaa;
    }



    public void seleccionarFechaEntrega()
    {
        fechaEntrega.setOnClickListener(v->{
            Calendar calendar = Calendar.getInstance();
            int dd= calendar.get(Calendar.DAY_OF_MONTH);
            int mm= calendar.get(Calendar.MONTH);
            int aaaa= calendar.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(CarritoVista.this,
                    android.R.style.Theme_DeviceDefault_Light_Dialog_Alert, dateSetListener,aaaa,mm,dd);
            datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            datePickerDialog.show();
        });

    }

    public void seleccionarHoraEntrega()
    {
        Calendar calendar = Calendar.getInstance();
        int hour =calendar.get(Calendar.HOUR);
        int min= calendar.get(Calendar.MINUTE);
        txtHoraEntrega.setOnClickListener(v->{

            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    android.R.style.Theme_Material_Light_Dialog_MinWidth, timeSetListener,hour,min, false);
            timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            timePickerDialog.show();

        });
    }

    public void setDataRecycler()
    {
        reciclador.setLayoutManager(manager);
        adaptadorItemCarrito = new AdaptadorItemCarrito(productosData, this);
        reciclador.setAdapter(adaptadorItemCarrito);
    }


    public void enviarPedido(PedidoCliente  pedidoCliente)
    {
        String urlBase="http://192.168.1.60:8080/BlumeServicioRestVentas/rest.envio.productos.pedidos.v1/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build();
        RestCliente restCliente = retrofit.create(RestCliente.class);

        Call<PedidoCliente> pedidoClienteCall = restCliente.enviarPedido(pedidoCliente);

        pedidoClienteCall.enqueue(new Callback<PedidoCliente>() {
            @Override
            public void onResponse(Call<PedidoCliente> call, Response<PedidoCliente> response) {
                if(response.isSuccessful())
                {
                    mensajeSimple("Pedido enviado");
                }
            }

            @Override
            public void onFailure(Call<PedidoCliente> call, Throwable t) {

                mensajeSimple("No se pudo enviar el pedido");

            }
        });

    }

    public  void enviarDetallePedidoProducto(DetalleProductoPedido detalleProductoPedido)
    {
        String urlBase="http://192.168.1.60:8080/BlumeServicioRestVentas/rest.envio.productos.pedidos.v1/";

        Retrofit retrofit = new Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build();
        RestCliente restCliente = retrofit.create(RestCliente.class);
        Call<DetalleProductoPedido> detalleProductoPedidoCall = restCliente.enviardetalleProductoPedido(detalleProductoPedido);

        detalleProductoPedidoCall.enqueue(new Callback<DetalleProductoPedido>() {
            @Override
            public void onResponse(Call<DetalleProductoPedido> call, Response<DetalleProductoPedido> response) {

            }

            @Override
            public void onFailure(Call<DetalleProductoPedido> call, Throwable t) {

            }
        });

    }

    public double devolverMenor(double a, double b, double c)
    {
        double menor=0.0;
        menor=a;
        if(b<menor && b>0.0)
        {
            menor=b;
        }
        else if(c<menor && c>0.0)
        {
            menor=c;
        }

        return menor;
    }

    public void mensaje(String msj)
    {
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.mensaje_gracias_por_su_compra, (ViewGroup) findViewById(R.id.mensaje_gracias));
        Toast toast = Toast.makeText(CarritoVista.this,msj, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0,0);
        toast.setView(view);
        toast.show();
    }

    public void mensajeSimple(String msj)
    {
        Toast.makeText(CarritoVista.this, msj, Toast.LENGTH_LONG).show();
    }
}
