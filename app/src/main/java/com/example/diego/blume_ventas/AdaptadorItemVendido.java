package com.example.diego.blume_ventas;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diego.blume_ventas.beans.Producto;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdaptadorItemVendido extends RecyclerView.Adapter<AdaptadorItemVendido.ProductoViewHolder> {

    private List<Producto> productos;
    private Context context;
    private View.OnClickListener listener;



    public AdaptadorItemVendido(List<Producto> productos, Context context)
    {
        this.productos=productos;
        this.context=context;
    }


    @NonNull
    @Override
    public AdaptadorItemVendido.ProductoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.modelo_item_compra, viewGroup, false);

        return new ProductoViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorItemVendido.ProductoViewHolder productoViewHolder, int i) {

        Picasso.get().load(productos.get(i).getFoto1()).into(productoViewHolder.imgProducto);
        productoViewHolder.txtNombre.setText(productos.get(i).getNombreProducto());
        productoViewHolder.txtCodigo.setText("Código:   "+productos.get(i).getCodigoProducto());
        productoViewHolder.txtPrecio.setText("Precio: S/. "+productos.get(i).getPrecioProducto());
        productoViewHolder.txtFechaProductoV.setText("Pedido el: "+ devolverMenor(productos.get(i).getPrecioProducto(),productos.get(i).getPrecioOferta(),productos.get(i).getPrecioOfertaDos()));
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public double devolverMenor(double a, double b, double c)
    {
        double menor=0.0;
        menor=a;
        if(b<menor && b>0.0)
        {
            menor=b;
        }
        else if(c<menor && c>0.0)
        {
            menor=c;
        }

        return menor;
    }

    public class ProductoViewHolder extends RecyclerView.ViewHolder {

        private TextView txtPrecio;
        private TextView txtCodigo;
        private TextView txtNombre;
        private ImageView imgProducto;
        private TextView txtFechaProductoV;


        public ProductoViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProducto = itemView.findViewById(R.id.img_item_carrito_v);
            txtNombre = itemView.findViewById(R.id.nombre_carito_producto_v);
            txtCodigo = itemView.findViewById(R.id.codigo_carrito_producto_v);
            txtPrecio = itemView.findViewById(R.id.precio_carrito_producto_v);
            txtFechaProductoV = itemView.findViewById(R.id.fecha_carrito_producto_v);
        }
    }
}
