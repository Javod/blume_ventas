package com.example.diego.blume_ventas;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.producto.viewmodel.UserViewModel;

import java.util.Calendar;

public class FormularioRegistro extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtCorreo;
    private EditText txtPasword;
    private EditText txtEdad;
    private Spinner opcSexo;
    private Button btnRegistrar;
    private User usuario;
    private UserViewModel userViewModel;
    private String opcionesSexo[]={"Mujer","Hombre","otro"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_registro);

        initcomponets();
        asignarDatosSpinner();
        verificarRegistroUsuario();


    }

    private void verificarRegistroUsuario()
    {
        userViewModel.getUserLiveData().observe(FormularioRegistro.this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {

                if(user!=null)
                {
                    txtNombre.setText(user.getNombreUsu());
                    txtCorreo.setText(user.getCorreo());
                    txtEdad.setText(String.valueOf(user.getEdadUsu()));


                    btnRegistrar.setOnClickListener(v->{

                        user.setNombreUsu(txtNombre.getText().toString());
                        user.setCorreo(txtCorreo.getText().toString());
                        user.setEdadUsu(Integer.parseInt(txtEdad.getText().toString()));
                        user.setGeneroUsu(opcSexo.getSelectedItem().toString());

                        userViewModel.updateUser(user);

                        Toast.makeText(FormularioRegistro.this, "Datos actualizados", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(FormularioRegistro.this, MainActivity.class));
                        finish();


                    });
                }
                else
                {
                    registarDatosDeUsuario();
                }

            }
        });
    }


    private void initcomponets()
    {
        txtNombre = findViewById(R.id.txt_nombre);
        txtCorreo = findViewById(R.id.txt_correo);
        txtPasword = findViewById(R.id.txt_password);
        txtEdad = findViewById(R.id.txt_edad);
        opcSexo = findViewById(R.id.opc_sexo);
        btnRegistrar = findViewById(R.id.btn_registrar);
        usuario = new User();
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    private void asignarDatosSpinner()
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,opcionesSexo);
        opcSexo.setAdapter(adapter);

    }



    private void registarDatosDeUsuario()
    {
        usuario.setIdUser(0);
        usuario.setStatusUsu("activo");
        usuario.setFechaRegistro(asignarfecha());

        btnRegistrar.setOnClickListener(v->{
            usuario.setNombreUsu(txtNombre.getText().toString());
            usuario.setCorreo(txtCorreo.getText().toString());
            usuario.setPassword(txtPasword.getText().toString());
            usuario.setEdadUsu(Integer.parseInt(txtEdad.getText().toString()));
            usuario.setGeneroUsu(opcSexo.getSelectedItem().toString());

            userViewModel.insertUser(usuario);

            startActivity(new Intent(FormularioRegistro.this, Login.class));
            finish();

        });

    }

    private String asignarfecha()
    {
        Calendar calendar = Calendar.getInstance();
        int dd= calendar.get(Calendar.DAY_OF_MONTH);
        int mm= calendar.get(Calendar.MONTH);
        int aaaa= calendar.get(Calendar.YEAR);

        return " "+dd+"/"+mm+"/"+aaaa;
    }
}
