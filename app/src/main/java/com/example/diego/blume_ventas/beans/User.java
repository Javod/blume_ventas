package com.example.diego.blume_ventas.beans;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName="user")
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "idUser")
    private int idUser;

    @NonNull
    @ColumnInfo(name = "nombreUsu")
    private String nombreUsu;

    @NonNull
    @ColumnInfo(name = "password")
    private String password;

    @NonNull
    @ColumnInfo(name = "correo")
    private String correo;

    @NonNull
    @ColumnInfo(name = "edadUsu")
    private int edadUsu;

    @NonNull
    @ColumnInfo(name = "fechaRegistro")
    private String fechaRegistro;

    @NonNull
    @ColumnInfo(name = "generoUsu")
    private String generoUsu;

    @NonNull
    @ColumnInfo(name = "statusUsu")
    private String statusUsu;

    public User()
    {

    }

    public User(@NonNull int idUser, @NonNull String nombreUsu, @NonNull String password, @NonNull String correo, @NonNull int edadUsu, @NonNull String fechaRegistro, @NonNull String generoUsu, @NonNull String statusUsu) {
        this.idUser = idUser;
        this.nombreUsu = nombreUsu;
        this.password = password;
        this.correo = correo;
        this.edadUsu = edadUsu;
        this.fechaRegistro = fechaRegistro;
        this.generoUsu = generoUsu;
        this.statusUsu = statusUsu;
    }

    @NonNull
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(@NonNull int idUser) {
        this.idUser = idUser;
    }

    @NonNull
    public String getNombreUsu() {
        return nombreUsu;
    }

    public void setNombreUsu(@NonNull String nombreUsu) {
        this.nombreUsu = nombreUsu;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(@NonNull String correo) {
        this.correo = correo;
    }

    @NonNull
    public int getEdadUsu() {
        return edadUsu;
    }

    public void setEdadUsu(@NonNull int edadUsu) {
        this.edadUsu = edadUsu;
    }

    @NonNull
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(@NonNull String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @NonNull
    public String getGeneroUsu() {
        return generoUsu;
    }

    public void setGeneroUsu(@NonNull String generoUsu) {
        this.generoUsu = generoUsu;
    }

    @NonNull
    public String getStatusUsu() {
        return statusUsu;
    }

    public void setStatusUsu(@NonNull String statusUsu) {
        this.statusUsu = statusUsu;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", nombreUsu='" + nombreUsu + '\'' +
                ", password='" + password + '\'' +
                ", correo='" + correo + '\'' +
                ", edadUsu=" + edadUsu +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", generoUsu='" + generoUsu + '\'' +
                ", statusUsu='" + statusUsu + '\'' +
                '}';
    }
}
