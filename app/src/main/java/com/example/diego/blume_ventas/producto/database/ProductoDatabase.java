package com.example.diego.blume_ventas.producto.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.diego.blume_ventas.beans.Direccion;
import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.beans.User;
import com.example.diego.blume_ventas.daos.DireccionDAO;
import com.example.diego.blume_ventas.daos.PedidoClienteDAO;
import com.example.diego.blume_ventas.daos.ProductoDAO;
import com.example.diego.blume_ventas.daos.UserDAO;

@Database(entities = {Producto.class,Direccion.class,PedidoCliente.class, User.class}, version = 132)
public abstract class ProductoDatabase extends RoomDatabase {

    public  abstract ProductoDAO productoDAO();
    public abstract DireccionDAO direccionDAO();
    public abstract PedidoClienteDAO pedidoClienteDAO();
    public abstract UserDAO userDAO();
    private static  volatile ProductoDatabase INSTANCE;

    public static ProductoDatabase getDatabase(final Context context)
    {
        if(INSTANCE==null)
        {
            synchronized (ProductoDatabase.class)
            {
                if(INSTANCE==null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ProductoDatabase.class,"producto_database").fallbackToDestructiveMigration().build();
                }
            }
        }

        return INSTANCE;
    }

}
