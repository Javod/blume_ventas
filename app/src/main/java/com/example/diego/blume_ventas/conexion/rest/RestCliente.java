package com.example.diego.blume_ventas.conexion.rest;

import com.example.diego.blume_ventas.beans.DetalleProductoPedido;
import com.example.diego.blume_ventas.beans.Direccion;
import com.example.diego.blume_ventas.beans.PedidoCliente;
import com.example.diego.blume_ventas.beans.Producto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RestCliente {

    String PUNTO_FINAL_PEDIDO="-----";
    String PUNTO_FINAL_DETALLE_PEDIDO="-----";
    String PUNTO_FINAL_PRODUCTO_PEDIDO="-----";

    @GET("productos")
    Call<List<Producto>> getProductos();

    @POST(PUNTO_FINAL_PEDIDO)
    Call<PedidoCliente> enviarPedido(@Body PedidoCliente pedidoCliente);


    @POST(PUNTO_FINAL_DETALLE_PEDIDO)
    Call<DetalleProductoPedido> enviardetalleProductoPedido(@Body DetalleProductoPedido detalleProductoPedido);

    @POST(PUNTO_FINAL_PRODUCTO_PEDIDO)
    Call<Producto> enviarProductoPedido(@Body Producto productoPedido);
}
