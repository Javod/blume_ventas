package com.example.diego.blume_ventas.producto.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.diego.blume_ventas.beans.Producto;
import com.example.diego.blume_ventas.daos.ProductoDAO;
import com.example.diego.blume_ventas.producto.database.ProductoDatabase;

import java.util.List;

public class ProductoRepository {

    private ProductoDAO productoDAO;
    private LiveData<List<Producto>> mAllProductos;

    public ProductoRepository(Application application)
    {
       ProductoDatabase db = ProductoDatabase.getDatabase(application);

       productoDAO = db.productoDAO();

       mAllProductos = productoDAO.findAll();
    }

    public LiveData<List<Producto>> getAllProductos()
    {
        return mAllProductos;
    }

    public void insertProducto(Producto producto)
    {
        new InsertAsyncTask(productoDAO).execute(producto);

    }

    public void deleteProducto(Producto producto)
    {
        new DeleteProductoTask(productoDAO).execute(producto);
    }

    public void deleteAllProductos()
    {
        new DeleteAllAsyncTask(productoDAO).execute();
    }

    public void updateProducto(Producto producto)
    {
        new UpdateProductoAsyncTask(productoDAO).execute(producto);
    }




    //Clases que se ejecutan en segundo plano

    private static  class  DeleteProductoTask extends  AsyncTask<Producto, Void, Void>
    {
        private ProductoDAO asyscProductoDAO;

        DeleteProductoTask(ProductoDAO asyscProductoDAO)
        {
            this.asyscProductoDAO = asyscProductoDAO;
        }

        @Override
        protected Void doInBackground(Producto... productos) {

            asyscProductoDAO.deleteProducto(productos[0]);

            return null;
        }
    }


    private static class InsertAsyncTask extends AsyncTask<Producto,Void,Void>
    {
        private  ProductoDAO asyncProductoDAO;

        InsertAsyncTask(ProductoDAO asyncProductoDAO)
        {
            this.asyncProductoDAO=asyncProductoDAO;
        }

        @Override
        protected Void doInBackground(Producto... productos) {

            asyncProductoDAO.insert(productos[0]);
            return null;
        }
    }

    private static  class DeleteAllAsyncTask extends AsyncTask<Void,Void,Void>
    {
        private ProductoDAO productoDAO;
        public DeleteAllAsyncTask(ProductoDAO productoDAO)
        {
           this.productoDAO = productoDAO;
        }


        @Override
        protected Void doInBackground(Void... voids) {

            productoDAO.deleteAll();

            return null;
        }
    }

    private static  class  UpdateProductoAsyncTask extends AsyncTask<Producto,Void,Void>
    {
        private ProductoDAO productoDAO;

        public UpdateProductoAsyncTask(ProductoDAO productoDAO)
        {
            this.productoDAO=productoDAO;
        }

        @Override
        protected Void doInBackground(Producto... productos) {

            productoDAO.updateProducto(productos[0]);

            return null;
        }
    }


}
